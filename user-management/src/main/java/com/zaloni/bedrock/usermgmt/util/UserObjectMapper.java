package com.zaloni.bedrock.usermgmt.util;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;
import com.zaloni.bedrock.usermgmt.dao.jpa.tables.User;

public class UserObjectMapper {

	public static UserDto convertToUserDto(User user) {

		return UserDto.builder().userId(user.getUserId()).password(user.getPassword())
				.enabled(user.isEnabled()).build();
	}
	
	
	public static User convertToUser(UserDto userDto) {
		return User.builder().userId(userDto.getUserId()).password(userDto.getPassword()).build();
	}

}
