package com.zaloni.bedrock.usermgmt.service.impl;

import java.util.List;
import java.util.Optional;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;
import com.zaloni.bedrock.usermgmt.api.service.UserManagementService;
import com.zaloni.bedrock.usermgmt.dao.jpa.repositories.UserRepository;
import com.zaloni.bedrock.usermgmt.dao.jpa.tables.User;
import com.zaloni.bedrock.usermgmt.util.UserObjectMapper;

@Service
@Transactional
public class UserManagementServiceImpl implements UserManagementService {

	@Autowired
	private UserRepository userRepository;

	public UserManagementServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDto saveUserDetails(UserDto user) {
		
		String encryptedPassword = new StrongPasswordEncryptor().encryptPassword(user.getPassword());
		user.setPassword(encryptedPassword);
		User userEntity = userRepository.saveAndFlush(UserObjectMapper.convertToUser(user));
		return UserObjectMapper.convertToUserDto(userEntity);

	}

	@Override
	@Transactional(readOnly = true)
	public UserDto getUserDetails(String userId) {
		Optional<User> user = userRepository.findById(userId);
		return UserObjectMapper
				.convertToUserDto(user.orElseThrow(() -> new RuntimeException("User with userId not found")));
	}

	@Override
	public List<UserDto> searchUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
