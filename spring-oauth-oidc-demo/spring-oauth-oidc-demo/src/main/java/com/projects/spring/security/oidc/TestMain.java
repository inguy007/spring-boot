package com.projects.spring.security.oidc;

import java.util.ArrayList;
import java.util.List;

public class TestMain {
	
	Object message() {
		return "Hello";
	}
	
	public static void main(String[] args) {
		int a=1;
		int b=0;
		//int c = a/b;
		//System.out.println(c);
		System.out.println(new TestMain().message());
		System.out.println("strawberries" instanceof String);
		
		System.out.println("apple".compareTo("banana"));
		
		List l = new ArrayList<>();
		l.add("Hello");
		l.add(2);
		System.out.println(l.get(0) instanceof Object);
		System.out.println(l.get(1) instanceof Integer);
		
	}

}
