package com.projects.spring.security.oidc.service;

import java.util.List;

import com.projects.spring.security.oidc.entity.User;

public interface UserService {

	void saveUser(User user);
	
	User getUser(String userId);
	
	void deleteUser(String userId);
	
	List<User> getUsers();
	
}
