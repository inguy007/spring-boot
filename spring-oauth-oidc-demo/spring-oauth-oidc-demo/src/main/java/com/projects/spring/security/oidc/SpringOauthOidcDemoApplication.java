package com.projects.spring.security.oidc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOauthOidcDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringOauthOidcDemoApplication.class, args);
	}

}
