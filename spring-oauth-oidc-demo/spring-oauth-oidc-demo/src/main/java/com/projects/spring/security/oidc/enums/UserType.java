package com.projects.spring.security.oidc.enums;

public enum UserType {

	INTERNAL, LDAP, EXTERNAL;

}
