package com.projects.spring.security.oidc.controller;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HomeController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

	//@Autowired
	private OAuth2AuthorizedClientService authorizedClientService;
	
	
	@GetMapping("/")
	public String homePath(Principal user) {
		LOGGER.debug("Principal object :{}",user);
		return "Hello " + user.getName();
	}
	
	 @RequestMapping("/me")
	   public ResponseEntity<Map> userinfo(OAuth2AuthenticationToken authentication) {
	       OAuth2AuthorizedClient authorizedClient = this.getAuthorizedClient(authentication);
	       Map<String,Object> userAttributes = Collections.emptyMap();
	       String userInfoEndpointUri = authorizedClient.getClientRegistration()
	               .getProviderDetails().getUserInfoEndpoint().getUri();
	       if (StringUtils.hasLength(userInfoEndpointUri)) {    // userInfoEndpointUri is optional for OIDC Clients
	    	  String accessToken = authorizedClient.getAccessToken().getTokenValue();
	    	  RestTemplate restTemplate = new RestTemplate();
	    	  
	    	  final HttpHeaders headers = new HttpHeaders();
	          headers.set(HttpHeaders.AUTHORIZATION, "Bearer "+accessToken);
	          LOGGER.info("TOKEN :{}",accessToken);
	          final HttpEntity<String> entity = new HttpEntity<String>(headers);
	          
	          ResponseEntity<Map> response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);        
	          
	    	  
	    	  userAttributes = response.getBody();
	       }
	      return ResponseEntity.ok(userAttributes);
	   }

	   private OAuth2AuthorizedClient getAuthorizedClient(OAuth2AuthenticationToken authentication) {
	       return this.authorizedClientService.loadAuthorizedClient(
	               authentication.getAuthorizedClientRegistrationId(), authentication.getName());
	   }
}
