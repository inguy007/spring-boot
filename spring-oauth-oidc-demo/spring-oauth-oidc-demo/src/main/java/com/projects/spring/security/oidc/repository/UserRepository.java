package com.projects.spring.security.oidc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projects.spring.security.oidc.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
