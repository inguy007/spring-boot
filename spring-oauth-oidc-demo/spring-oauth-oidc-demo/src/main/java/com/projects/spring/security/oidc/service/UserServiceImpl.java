package com.projects.spring.security.oidc.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projects.spring.security.oidc.entity.User;
import com.projects.spring.security.oidc.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void saveUser(User user) {
		
		// TODO Auto-generated method stub
		userRepository.save(user);
	}

	@Override
	public User getUser(String userId) {
		return userRepository.getOne(userId);
	}

	@Override
	public void deleteUser(String userId) {
		userRepository.deleteById(userId);
	}

	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}

}
