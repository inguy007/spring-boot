package com.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaDiscoveryClientConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories(basePackages = {"com.test.springboot.repository"})
//@SpringBootApplication(exclude = {WebMvcAutoConfiguration.class,EurekaClientAutoConfiguration.class,EurekaDiscoveryClientConfiguration.class})
//@ComponentScan({"com.test"})
public class ShellApplication {
	
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ShellApplication.class);
		app.setWebApplicationType(WebApplicationType.NONE);
		app.run(args);
	}

}
