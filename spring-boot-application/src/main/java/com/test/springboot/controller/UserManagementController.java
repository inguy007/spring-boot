package com.test.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;
import com.zaloni.bedrock.usermgmt.api.service.UserManagementService;

@RestController
@RequestMapping("/users")
public class UserManagementController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementController.class);
	
	@Autowired
	private UserManagementService userManagementService;
	
	@Secured({"manage"})
	@RequestMapping(value = "/register",method = RequestMethod.POST,consumes="application/json")
	public ResponseEntity<EntityModel<UserDto>> registerUser(@RequestBody UserDto user) {
		UserDto userObj = userManagementService.saveUserDetails(user);
		EntityModel<UserDto> userEntityModel = EntityModel.of(userObj);
		userEntityModel.add(WebMvcLinkBuilder.linkTo(getClass()).slash(userObj.getUserId()).withSelfRel());
		return ResponseEntity.created(WebMvcLinkBuilder.linkTo(getClass()).slash(userObj.getUserId()).toUri()).body(userEntityModel);
	}
	
	@Secured({"view","manage"})
	@RequestMapping(value="/{userId}",method= RequestMethod.GET,produces="application/json")
	public EntityModel<UserDto> getUserDetaisl(@PathVariable String userId){
		UserDto user = userManagementService.getUserDetails(userId);
		EntityModel<UserDto> entityModel = EntityModel.of(user);
		Link userResourceLink = WebMvcLinkBuilder.linkTo(UserManagementController.class).slash(userId).withSelfRel();
		entityModel.add(userResourceLink);
		return entityModel;
	}
	
	@Secured({"view","manage"})
	@RequestMapping(value="/current",method=RequestMethod.GET,produces="application/json")
	public EntityModel<Object> getCurrentUser(){
		return EntityModel.of(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
	}
	
}
