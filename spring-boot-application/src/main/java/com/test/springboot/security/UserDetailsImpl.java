package com.test.springboot.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;

public class UserDetailsImpl implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 989666255665536089L;
	private UserDto user;

	public UserDetailsImpl(UserDto user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		authorities.add(new SimpleGrantedAuthority("USER"));
		return authorities;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getUserId();
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.user.isEnabled();
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.user.isEnabled();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.user.isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return this.user.isEnabled();
	}

}
