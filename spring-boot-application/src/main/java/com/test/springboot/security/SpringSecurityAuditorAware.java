package com.test.springboot.security;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import io.jsonwebtoken.Claims;

public class SpringSecurityAuditorAware implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		Claims jwtClaims = (Claims) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return Optional.ofNullable(jwtClaims.getSubject());
	}

}
