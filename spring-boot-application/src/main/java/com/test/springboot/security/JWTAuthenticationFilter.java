package com.test.springboot.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

	private static final String AUTH_HEADER = "Authorization";

	private AuthenticationManager authenticationManager;

	private Environment env;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, Environment env) {
		super();
		this.authenticationManager = authenticationManager;
		this.env = env;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			LoginRequest loginRequest = new ObjectMapper().readValue(request.getInputStream(), LoginRequest.class);
			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		String token = Jwts.builder().setSubject(((UserDetailsImpl) authResult.getPrincipal()).getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000))
				.signWith(SignatureAlgorithm.HS512, env.getProperty("jwt.secret")).setIssuedAt(new Date()).compact();
		response.addHeader(AUTH_HEADER, "Bearer " + token);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		LOGGER.debug("Entered the filter to validate REST call");
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		LOGGER.debug(" URL called : {}",httpServletRequest.getRequestURI());
		Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		if(auth2 != null) {
			LOGGER.debug("Authentication details : {}",auth2);
			LOGGER.debug("AUTH OBJ :[obj={},principal={}]",auth2.getDetails(),auth2.getPrincipal());
		}
		if(httpServletRequest.getRequestURI().equalsIgnoreCase("/demoapp/login")) {
			
			Authentication auth = attemptAuthentication(httpServletRequest,(HttpServletResponse) response);
			if(auth != null) {
				LOGGER.debug("successfully authenticated");
				successfulAuthentication(httpServletRequest,(HttpServletResponse) response, filterChain, auth);
				return;
			}
		}
		String token = extractJwtToken(httpServletRequest);
		if (token != null) {

			Claims user = Jwts.parser().setSigningKey(env.getProperty("jwt.secret")).parseClaimsJws(token).getBody();
			
			if (user != null) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null,
						new ArrayList<>());
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}

		}else {
			LOGGER.debug("Token is not available");
		}
		filterChain.doFilter(request, response);
	}

	private String extractJwtToken(HttpServletRequest request) {

		String bearerToken = request.getHeader(AUTH_HEADER);
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

}
