package com.test.springboot.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;

public class CustomAccessDecisionManager implements AccessDecisionManager{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomAccessDecisionManager.class);

	@Override
	public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
			throws AccessDeniedException, InsufficientAuthenticationException {
		// TODO Auto-generated method stub
		LOGGER.info("Invoked decision manager");
		String userId = authentication.getName();
		LOGGER.info("User information :{}",userId);
		if(!configAttributes.isEmpty()) {
			configAttributes.stream().forEach(configAttribute->{
				String permission = configAttribute.getAttribute();
				LOGGER.info("Permission assigned :{}",permission);
			});
		}
		
	}

	@Override
	public boolean supports(ConfigAttribute attribute) {
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
