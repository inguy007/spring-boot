package com.test.springboot.security;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;
import com.zaloni.bedrock.usermgmt.api.service.UserManagementService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	
	@Autowired
	private UserManagementService userManagementService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOGGER.debug("Loading user details for username:{}",username);
		UserDto user = userManagementService.getUserDetails(username);
		if(user != null) {
			return new UserDetailsImpl(user);
		}
		LOGGER.error("User details not found");
		throw new UsernameNotFoundException("User '"+username+"' not found");
	}

}
