package com.test.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.test.springboot.security.JWTAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Order(101)
public class CustomSecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private DaoAuthenticationProvider authenticationProvider;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private Environment env;
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(authenticationProvider);
//    	auth
//          .inMemoryAuthentication()
//          .withUser("user")
//          .password(encoder.encode("password"))
//          .roles("USER")
//          .and()
//          .withUser("admin")
//          .password(encoder.encode("admin"))
//          .roles("USER", "ADMIN");
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.cors().and().csrf().disable().authorizeRequests()
    	  .antMatchers("/", "/login**","/callback/", "/webjars/**", "/error**")
          .permitAll()
          .anyRequest()
          .authenticated()
          .and()
          .addFilter(new JWTAuthenticationFilter(authenticationManager, env))
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

}
