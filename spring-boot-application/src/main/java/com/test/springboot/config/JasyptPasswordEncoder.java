package com.test.springboot.config;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;

public class JasyptPasswordEncoder implements PasswordEncoder{

	private StrongPasswordEncryptor passwordEncryptor;
	
	public JasyptPasswordEncoder() {
		this.passwordEncryptor = new StrongPasswordEncryptor();
	}

	@Override
	public String encode(CharSequence rawPassword) {
		return passwordEncryptor.encryptPassword(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encryptedPassword) {
		return passwordEncryptor.checkPassword(rawPassword.toString(), encryptedPassword);
	}
	
}
