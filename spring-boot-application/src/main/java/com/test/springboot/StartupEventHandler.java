package com.test.springboot;



import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.test.springboot.config.MetricsConfig;
import com.zaxxer.hikari.HikariDataSource;

import io.micrometer.core.instrument.Measurement;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.jmx.JmxMeterRegistry;

@Component
public class StartupEventHandler {

   
    private static Logger LOGGER = LoggerFactory.getLogger(StartupEventHandler.class);
   
   
	//@Autowired
	private DataSource dataSource;
    
	@Autowired
    public StartupEventHandler(MeterRegistry registry,DataSource dataSource) {
        this.meterRegistry = registry;
        this.dataSource=dataSource;
    }
    
    private MeterRegistry meterRegistry;

    @EventListener
    public void getAndLogStartupMetrics(
      ApplicationReadyEvent event) {
       getAndLogActuatorMetric();
    }

    private void getAndLogActuatorMetric() {
    	//HikariDataSource ds = (HikariDataSource) dataSource; 
    	
    	meterRegistry.forEachMeter(meter->
        {
        	Iterable<Measurement> measurement = meter.measure();
        	measurement.forEach(measure->{
        		//LOGGER.info("METRICS MEASUREMENT [metric={},statistics={},measure={}]",meter.getId(),measure.getStatistic(),measure.getValue());
        	});
        });
        
    }
}