package com.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EurekaDiscoveryClientConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class, EurekaClientAutoConfiguration.class,
		EurekaDiscoveryClientConfiguration.class, RefreshAutoConfiguration.class })
@EnableJpaRepositories(basePackages = { "com.test.springboot.repository",
		"com.zaloni.bedrock.usermgmt.dao.jpa.repositories" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.test", "com.zaloni" })
@EntityScan(basePackages = { "com.test", "com.zaloni" })
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorProvider")
@EnableOAuth2Sso
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
