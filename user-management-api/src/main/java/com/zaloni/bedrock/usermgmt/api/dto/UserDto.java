package com.zaloni.bedrock.usermgmt.api.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
	
	private String userId;
	private String password;
	private boolean enabled;
	private String role;
	private Date createdDate;
    private Date modifiedDate;
    private String createdBy;
    private String modifiedBy;

}
