package com.zaloni.bedrock.usermgmt.api.service;

import java.util.List;

import com.zaloni.bedrock.usermgmt.api.dto.UserDto;

public interface UserManagementService {

	UserDto saveUserDetails(UserDto user);

	UserDto getUserDetails(String userId);

	List<UserDto> searchUsers();

}
