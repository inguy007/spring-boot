package com.projects.spring.security.saml.controller;

import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticatedPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

	@GetMapping("/")
	public String homePath(Principal user) {
		LOGGER.debug("Principal object :{}", ((Saml2Authentication)user).getPrincipal());
		return "Hello " + user.getName();
	}

	@RequestMapping("/me")
	public ResponseEntity<Map<String,Object>> userinfo(Principal principal) {
		Map<String, Object> userAttributes = new HashMap<>();
		Saml2Authentication samlPrincipal = (Saml2Authentication) principal;
		userAttributes.put("SAML_USER", samlPrincipal.getPrincipal());
		userAttributes.putAll(((Saml2AuthenticatedPrincipal)samlPrincipal.getPrincipal()).getAttributes());
		userAttributes.put("SAML_RESPONSE", samlPrincipal.getSaml2Response());
		
		return ResponseEntity.ok(userAttributes);
	}
	
	
	

}
