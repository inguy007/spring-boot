package com.projects.spring.security.saml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSamlDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSamlDemoApplication.class, args);
	}

}
