package com.zaloni.bedrock.usermgmt.dao.jpa.tables;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.zaloni.bedrock.usermgmt.dao.jpa.listeners.RoleEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(value = { RoleEntityListener.class })
@Entity
@Table(name="ROLE")
public class Role implements Serializable{

	private static final long serialVersionUID = 5538195409112895940L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ROLE_ID")
	private Long roleId;
	
	@Column(name="ROLE_NAME")
	private String roleName;
	
	@OneToMany(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ROLE_ID",referencedColumnName = "ROLE_ID")
	private Set<Permission> permissions;
}
