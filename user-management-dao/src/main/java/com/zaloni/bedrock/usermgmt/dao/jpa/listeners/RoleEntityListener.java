package com.zaloni.bedrock.usermgmt.dao.jpa.listeners;

import javax.persistence.PreRemove;

public class RoleEntityListener {
	
	@PreRemove
	public void beforeDelete(Object object) {
		//do something to check before the object is removed from DB
		
	}

}
