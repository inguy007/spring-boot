package com.zaloni.bedrock.usermgmt.dao.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zaloni.bedrock.usermgmt.dao.jpa.tables.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
