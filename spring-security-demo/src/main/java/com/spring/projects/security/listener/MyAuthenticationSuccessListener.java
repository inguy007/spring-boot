package com.spring.projects.security.listener;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.spring.projects.security.utils.JwtUtils;

@Component
public class MyAuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {

	@Autowired
	private HttpSession httpSession;
	
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        System.out.println("Authenticated :"+event.getAuthentication());
        SecurityContextHolder.getContext().setAuthentication(event.getAuthentication());
        //String token = JwtUtils.createToken(event.getAuthentication().getName());
       // System.out.println("Token generated :"+token);
       // System.out.println("HTTP SERVLET CONTEXT :"+httpSession.getServletContext());
       // HttpServletRequest curRequest = 
        //		((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
       // 		.getRequest();
       // System.out.println("CURRENT HTTP REQUEST :"+curRequest.getRequestURI());
        
        if(event.getAuthentication() instanceof OAuth2LoginAuthenticationToken) {
        	OAuth2LoginAuthenticationToken oauth2LoginToken = (OAuth2LoginAuthenticationToken) event.getAuthentication();
        //	System.out.println("ACCESS TOKEN: " +oauth2LoginToken.getAccessToken().getTokenValue());
        //	System.out.println("USER ATTRIBUTES: "+oauth2LoginToken.getPrincipal().getAttributes());
        }
        
        

    }

 
}
