package com.spring.projects.security.filter;

import com.nimbusds.oauth2.sdk.util.StringUtils;
import com.spring.projects.security.auth.UserDetailsServiceImpl;
import com.spring.projects.security.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class JwtValidationFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtValidationFilter.class);

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    @Qualifier("compositeJwtDecoder")
    private JwtDecoder jwtDecoder;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
       
    	System.out.println("Request URL "+httpServletRequest.getRequestURI());
        if (!httpServletRequest.getRequestURI().startsWith("/spring-security/login") && !httpServletRequest.getRequestURI().contains("oauth2")) {
            System.out.println("Request URL " + httpServletRequest.getContextPath());
            String authorizationHeader = httpServletRequest.getHeader("Authorization");
            String jwtToken = null;
            String username = null;
            String[] clientScope = null;
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
                jwtToken = authorizationHeader.substring(7);
                if (StringUtils.isNotBlank(jwtToken)) {
                    Map<String, Object> claims = null;
                    try {
                        claims = jwtDecoder.decode(jwtToken).getClaims();
                        username = claims.get("sub") != null ? String.valueOf(claims.get("sub")) : null;
                        clientScope = claims.get("scp") != null ? claims.get("scp") instanceof String[] ? (String[])claims.get("scp") : new String[]{String.valueOf(claims.get("scp"))} : null;
                    } catch (JwtException e) {
                        LOGGER.info("JWT Token could not be parsed through external decoder", e);
                    }
                    LOGGER.info("CLAIMS : {}", claims);
                }
            }
        /*if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if(JwtUtils.validateToken(jwtToken,userDetails)){
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails.getUsername(),null,userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }*/
        } filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
