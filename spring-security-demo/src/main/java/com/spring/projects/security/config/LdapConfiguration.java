package com.spring.projects.security.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class LdapConfiguration {

	//@Value("${spring.ldap.urls}")
	//private String[] ldapUrls;
	
	@Value("${spring.ldap.base}")
	private String base;
	
	@Value("${spring.ldap.username}")
	private String userDn;
	
	@Value("${spring.ldap.password}")
	private String password;
	
	
	@Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        //contextSource.setUrls(ldapUrls);
        contextSource.setUrl("ldap://192.168.1.194:389");
        contextSource.setBase(base);
        contextSource.setUserDn(userDn);
        contextSource.setPassword(password);
        return contextSource;
    }
	
	@Bean
	public LdapTemplate ldapTemplate() {
	    return new LdapTemplate(contextSource());
	}
}
