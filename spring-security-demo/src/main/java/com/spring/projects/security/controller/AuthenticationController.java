package com.spring.projects.security.controller;

import com.spring.projects.security.auth.CustomLdapAuthenticationProvider;
import com.spring.projects.security.utils.JwtUtils;
import com.spring.projects.security.web.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthenticationController{

    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;
    
    @Autowired
    private CustomLdapAuthenticationProvider ldapAuthenticationProvider;
    
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    

    @PostMapping("/authenticate")
    public ResponseEntity<String> authenticateUser(@RequestBody AuthRequest authRequest, HttpServletResponse response){
        Authentication authentication = null;
        try{
        	String user = authRequest.getUsername();
        	if(user.equalsIgnoreCase("admin")) {
        		authentication = daoAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        	}else {
        		authentication = ldapAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        	}
         }catch (BadCredentialsException e){
            throw new RuntimeException("Incorrect username or password",e);
        }
        if(authentication.isAuthenticated()){
            String token = JwtUtils.createToken(authentication.getName());
            response.setHeader("Authorization","Bearer "+token);
            applicationEventPublisher.publishEvent(new AuthenticationSuccessEvent(authentication));
            return ResponseEntity.ok("User authenticated successfully");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error authenticating user");
    }

}
