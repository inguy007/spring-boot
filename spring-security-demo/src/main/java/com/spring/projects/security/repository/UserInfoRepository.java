package com.spring.projects.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.projects.security.entities.UserAuthType;
import com.spring.projects.security.entities.UserInfo;

@Repository
@Transactional
public interface UserInfoRepository extends JpaRepository<UserInfo,String> {
	
	public UserInfo findByUsername(String username);
	
	public UserInfo findByUsernameAndAuthType(String username,UserAuthType authType);
}
