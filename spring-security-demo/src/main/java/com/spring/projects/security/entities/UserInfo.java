package com.spring.projects.security.entities;

import javax.persistence.*;

@Entity
@Table(name="USER_INFO")
public class UserInfo {

    @Id
    @Column(name="USER_NAME")
    private String username;
    
    @Column(name="PASSWORD")
    private String password;
    
    @Column(name="IS_ACTIVE")
    private Boolean active;
    
    @Column(name="AUTH_TYPE")
    @Enumerated(EnumType.STRING)
    private UserAuthType authType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

	public UserAuthType getAuthType() {
		return authType;
	}

	public void setAuthType(UserAuthType authType) {
		this.authType = authType;
	}
    
    
}
