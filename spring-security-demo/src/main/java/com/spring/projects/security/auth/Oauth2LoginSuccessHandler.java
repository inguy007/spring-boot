package com.spring.projects.security.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.spring.projects.security.entities.UserAuthType;
import com.spring.projects.security.entities.UserInfo;
import com.spring.projects.security.repository.UserInfoRepository;
import com.spring.projects.security.utils.JwtUtils;

@Component
public class Oauth2LoginSuccessHandler implements AuthenticationSuccessHandler{

	@Autowired
	private UserInfoRepository userInfoRepository;
	
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		System.out.println("Login Success Handler called :"+authentication);
		if(authentication instanceof OAuth2AuthenticationToken) {
        	OAuth2AuthenticationToken oauth2LoginToken = (OAuth2AuthenticationToken) authentication;
        	System.out.println("USER ATTRIBUTES: "+oauth2LoginToken.getPrincipal().getAttributes());
        	DefaultOidcUser oidcUser = (DefaultOidcUser) oauth2LoginToken.getPrincipal();
        	String username = oidcUser.getAttribute("preferred_username");
        	UserInfo userInfo = userInfoRepository.findByUsernameAndAuthType(username,UserAuthType.OAUTH2);
        	if(userInfo == null) {
        		System.out.println("Adding new oauth2 user profile");
        		UserInfo newUserInfo = new UserInfo();
        		newUserInfo.setUsername(username);
        		newUserInfo.setAuthType(UserAuthType.OAUTH2);
        		newUserInfo.setActive(true);
        		//role mappings
        		userInfoRepository.save(newUserInfo);
        	}
        }
		String generatedJwtToken = JwtUtils.createToken(authentication.getName());
		response.setHeader("Authorization", "Bearer "+generatedJwtToken);
		response.sendRedirect(request.getContextPath());
	}

}
