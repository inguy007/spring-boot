package com.spring.projects.security.auth;

import com.spring.projects.security.entities.UserAuthType;
import com.spring.projects.security.entities.UserInfo;
import com.spring.projects.security.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
@Transactional(readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserInfoRepository userInfoRepository;

    @Autowired
    public UserDetailsServiceImpl(UserInfoRepository userInfoRepository){
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo userInfo = userInfoRepository.findByUsername(username);
        if(userInfo == null){
            throw new UsernameNotFoundException("Error fetching user details");
        }
        return getUser(userInfo);
    }

    private static User getUser(UserInfo userInfo){
    	String password = "";
    	if(userInfo.getAuthType() == UserAuthType.INTERNAL) {
    		password = userInfo.getPassword();
    	}
        return new User(userInfo.getUsername(),password, Collections.emptyList());
    }
}
