package com.spring.projects.security.config;

import com.spring.projects.security.auth.CustomLdapAuthenticationProvider;
import com.spring.projects.security.auth.Oauth2LoginSuccessHandler;
import com.spring.projects.security.auth.UserDetailsServiceImpl;
import com.spring.projects.security.filter.JwtValidationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailServiceImpl;

    @Autowired
    private JwtValidationFilter jwtValidationFilter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private OAuth2UserService oAuth2UserService;

    @Autowired
    private OAuth2AccessTokenResponseClient oAuth2AccessTokenResponseClient;
    
    @Autowired
    private CustomLdapAuthenticationProvider ldapAuthenticationProvider;

    @Autowired
    private Oauth2LoginSuccessHandler oauth2LoginSuccessHandler;

//    @Bean
//    public AuthenticationManager authenticationManager() {
//        List<AuthenticationProvider> authenticationProviders = new ArrayList<AuthenticationProvider>();
//        authenticationProviders.add(daoAuthenticationProvider());
//        authenticationProviders.add(new OAuth2LoginAuthenticationProvider(oAuth2AccessTokenResponseClient, oAuth2UserService));
//        authenticationProviders.add(ldapAuthenticationProvider);
//        return new ProviderManager(authenticationProviders);
//    }


    @Override
    @Bean //required since Spring-Boot 2.0
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(daoAuthenticationProvider())
//                .authenticationProvider(new OAuth2LoginAuthenticationProvider(oAuth2AccessTokenResponseClient, oAuth2UserService))
//                .authenticationProvider(ldapAuthenticationProvider);
//                
//        super.configure(auth);
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests().antMatchers("/authenticate", "/oauth**", "/login","/").permitAll()
                .anyRequest().authenticated()
                .and()
                .oauth2Login().successHandler(oauth2LoginSuccessHandler)
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().oauth2ResourceServer().jwt();
        http.addFilterBefore(jwtValidationFilter, OAuth2AuthorizationRequestRedirectFilter.class);
    }
}
