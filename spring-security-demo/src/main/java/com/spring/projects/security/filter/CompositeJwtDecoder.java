package com.spring.projects.security.filter;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.crypto.SecretKey;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Component
public class CompositeJwtDecoder implements JwtDecoder {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompositeJwtDecoder.class);

    private static String SECRET_KEY = "secret";

    @Autowired
    private InMemoryClientRegistrationRepository clientRegistrationRepository;

    private List<JwtDecoder> jwtDecoderList;

    private List<JwtDecoder> prepareDecoderList(){
        List<JwtDecoder> jwtDecoderList = new ArrayList<>();
        clientRegistrationRepository.forEach(clientRegistration -> {
            String jwkSetUri = clientRegistration.getProviderDetails().getJwkSetUri();
            jwtDecoderList.add(NimbusJwtDecoder.withJwkSetUri(jwkSetUri).build());
        });
        jwtDecoderList.add((input) -> {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(input);
            Instant issuedAt = String.valueOf(claimsJws.getBody().get("iat")) != null ? Instant.ofEpochMilli(Long.parseLong(String.valueOf(claimsJws.getBody().get("iat")))) : null;
            Instant expiresAt = String.valueOf(claimsJws.getBody().get("exp")) != null ? Instant.ofEpochMilli(Long.parseLong(String.valueOf(claimsJws.getBody().get("exp")))) : null;
            return Jwt.withTokenValue(input).headers((h) -> {
                h.putAll(claimsJws.getHeader());
            }).claims((c) -> {
                c.putAll(claimsJws.getBody());
            }).expiresAt(expiresAt).issuedAt(issuedAt).build();
        });
        return jwtDecoderList;
    }

    @Override
    public Jwt decode(String s) throws JwtException {
        if(CollectionUtils.isEmpty(jwtDecoderList)){
            jwtDecoderList = prepareDecoderList();
        }
        Jwt jwt = null;
        for(JwtDecoder jwtDecoder : jwtDecoderList){
            try{
                jwt = jwtDecoder.decode(s);
            }catch(Exception e){
                LOGGER.debug("Error parsing jwt token",e);
                continue;
            }

        }
        return jwt;
    }
}
