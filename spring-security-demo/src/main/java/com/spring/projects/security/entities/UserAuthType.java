package com.spring.projects.security.entities;

public enum UserAuthType {
	
	INTERNAL, LDAP, OAUTH2, SAML2;

}
