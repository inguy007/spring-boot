package com.spring.projects.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CustomLdapAuthenticationProvider implements AuthenticationProvider{

	 private static final Logger LOGGER = LoggerFactory.getLogger(CustomLdapAuthenticationProvider.class);

     @Autowired
     private LdapTemplate ldapTemplate;
	 
     @Override
     public Authentication authenticate(Authentication authentication) {
    	  System.out.println("Attempting LDAP authentication");
          UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
          String username = authentication.getName();
          String rawPassword = (String) authentication.getCredentials();
          System.out.println("Usernmae : "+username);
          System.out.println("Password :"+rawPassword);
          if (StringUtils.isEmpty(username) || StringUtils.isEmpty(rawPassword)) {
               throw new BadCredentialsException("Either the username or password is not provided.");
          }

          try {
               ldapTemplate.getContextSource().getContext(username, rawPassword);
               usernamePasswordAuthenticationToken =
                         new UsernamePasswordAuthenticationToken(username, "", authentication.getAuthorities());
          } catch (org.springframework.ldap.AuthenticationException ae) {
               LOGGER.warn("Error {} occurred while ldap authentication", ae);
               throw new BadCredentialsException("Ldap authentication Failed.");
          } catch (Exception e) {
               LOGGER.info("Error {} occurred while loading ldap configuration", e);
               throw new BadCredentialsException("Error loading LDAP configuration");
          }
          return usernamePasswordAuthenticationToken;
     }

    

     @Override
     public boolean supports(Class<?> authentication) {

          if (authentication.getName().equalsIgnoreCase(UsernamePasswordAuthenticationToken.class.getName()))
               return true;
          return false;
     }

	
}
