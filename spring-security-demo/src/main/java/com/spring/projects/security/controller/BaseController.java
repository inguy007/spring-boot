package com.spring.projects.security.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    @GetMapping("/me")
    public ResponseEntity<String> userinfo(Authentication authentication,Principal principal){
    	if(principal != null) {
    		LOGGER.info("Principal Type :"+principal.getClass());
    		if(authentication != null) {
        		LOGGER.info("Authentication type :"+authentication.getClass());
        		//return ResponseEntity.ok(authentication.getName());
        	}
    		return ResponseEntity.ok("Hello "+principal.getName());
    	}
    	if(authentication != null) {
    		LOGGER.info("Authentication type :"+authentication.getClass());
    		return ResponseEntity.ok(authentication.getName());
    	}
    	return ResponseEntity.ok("Hello Unknown User");
    }
    
    @GetMapping("/")
    public ResponseEntity<String> home(Authentication authentication,Principal principal){
//    	if(principal != null) {
//    		LOGGER.info("Principal Type :"+principal.getClass());
//    		if(authentication != null) {
//        		LOGGER.info("Authentication type :"+authentication.getClass());
//        		//return ResponseEntity.ok(authentication.getName());
//        	}
//    		return ResponseEntity.ok("Hello "+principal.getName());
//    	}
//    	if(authentication != null) {
//    		LOGGER.info("Authentication type :"+authentication.getClass());
//    		return ResponseEntity.ok(authentication.getName());
//    	}
//    	return ResponseEntity.ok("Hello Unknown User");
    	System.out.println("AUTH OBJ "+authentication);
    	return ResponseEntity.ok("Welcome to secure application");
    }
    
    @GetMapping("/secured")
    public ResponseEntity<String> securedMessage(){
    	return ResponseEntity.ok("This is a secured message");
    }
}
